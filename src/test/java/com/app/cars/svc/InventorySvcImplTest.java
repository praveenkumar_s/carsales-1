package com.app.cars.svc;

import com.app.cars.beans.Car;
import com.app.cars.beans.CarModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Created by praveen on 1/17/2016.
 */
public class InventorySvcImplTest {

    private InventorySvcImpl svc;

    @Before
    public void setUp() throws Exception {
        svc = new InventorySvcImpl();
    }

    public void searchModelShouldThrowExceptionWhenNoModelDataIsPresent() {
        List<Car> list = svc.searchCarModels("test");
        assertNull(list);
    }

    @Test
    public void searchModelShouldReturnModelWhenFound() {
        Car car = createCar("White", 2013, "Toyota", "Camry", "Sedan");
        List<Car> cars = new ArrayList<Car>();
        cars.add(car);
        svc.setCars(cars);

        List<Car> carList = svc.searchCarModels("Camry");
        assertThat(carList.size(),is(1));
    }

    private Car createCar(String color, int year, String make, String model, String type) {
        Car car = new Car();
        car.setColor(color);
        car.setYear(year);
        CarModel carMode = new CarModel();
        carMode.setMake(make);
        carMode.setModel(model);
        carMode.setType(type);
        car.setModel(carMode);
        return car;
    }

}